package main

import (
    "bytes"
)

func main() {
    src := make([]byte, 1024 * 10)
    //dst := make([]byte, 1024 * 10)
    for i, _ := range src {
        src[i] = 'a'
    }

    //srcBuf = NewBuffer(src)
    //dstBuf = NewBuffer(dst)
    dst := bytes.NewBuffer(make([]byte, 1024 * 10))

    for i := 0; i < 10000000; i++ {
        dst.Reset()
        dst.Write(src)
    }
}