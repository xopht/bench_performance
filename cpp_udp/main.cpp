#include <boost/asio.hpp>
#include <iostream>

using namespace std;
using boost::asio::ip::udp;


boost::asio::io_service io_service;
udp::socket udp_socket(io_service, udp::endpoint(udp::v4(), 33333));
udp::endpoint remote_endpoint;
udp::endpoint client_endpoint(udp::v4(), 33335);
char buffer[30000] = {0, };

void handle_send(
    const boost::system::error_code& ec,
    std::size_t bytes_transferred)
{
    //std::cout << "send:" << bytes_transferred << std::endl;
}

void handle_receive(const boost::system::error_code& error, std::size_t bytes_transferred)
{
    //std::cout << "recv:" << bytes_transferred << std::endl;
    udp_socket.async_send_to(boost::asio::buffer(buffer, bytes_transferred), client_endpoint, handle_send);

    udp_socket.async_receive_from(boost::asio::buffer(buffer, 30000), remote_endpoint, handle_receive);
}


int main()
{
    udp_socket.async_receive_from(boost::asio::buffer(buffer, 30000), remote_endpoint, handle_receive);

    io_service.run();
}
