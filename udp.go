package main
 
import (
    "fmt"
    "net"
    //"time"
    //"strconv"
    "os"
)
 
func CheckError(err error) {
    if err  != nil {
        fmt.Println("Error: " , err)
        os.Exit(1)
    }
}
 
func main() {
    ServerAddr,err := net.ResolveUDPAddr("udp","127.0.0.1:33333")
    CheckError(err)
 
    LocalAddr, err := net.ResolveUDPAddr("udp", "127.0.0.1:33335")
    CheckError(err)

    server, err := net.ListenUDP("udp", ServerAddr)
    CheckError(err)

    defer server.Close()

    buffer := make([]byte, 30000);
    for {
        n, _ := server.Read(buffer)
        server.WriteTo(buffer[:n], LocalAddr)
    }
}