var PORT = 33333;
var HOST = '127.0.0.1';

var dgram = require('dgram');
var server = dgram.createSocket('udp4');

server.on('listening', function () {
    var address = server.address();
    console.log('UDP Server listening on ' + address.address + ":" + address.port);
});

server.on('message', function (message, remote) {
    //console.log(remote.address + ':' + remote.port +' - ' + message);
    //server.send(message, 0, message.length, remote.port, remote.address);
    server.send(message, 0, message.length, 33335, "127.0.0.1")
});

server.bind(PORT, HOST);