package main

func main() {
    
    src := make([]byte, 1024 * 10)
    dst := make([]byte, 1024 * 10)
    for i, _ := range src {
        src[i] = 'a'
    }

    for i := 0; i < 10000000; i++ {
        copy(dst, src)
    }
}