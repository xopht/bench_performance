{
    "targets": [
        {
            "target_name": "asio",
            "sources": ["asio.cc"],
            "cflags_cc!": [ "-fno-rtti", "-fno-exceptions" ],
            "cflags!": [ "-fno-exceptions" ],
            "conditions": [
                [ 'OS=="mac"', {
                    "xcode_settings": {
                        'OTHER_CPLUSPLUSFLAGS' : ['-std=c++11','-stdlib=libc++', '-v'],
                        'OTHER_LDFLAGS': ['-stdlib=libc++'],
                        'MACOSX_DEPLOYMENT_TARGET': '10.10',
                        'GCC_ENABLE_CPP_EXCEPTIONS': 'YES'
                    },

                    "link_settings": {
                            "libraries": [
                                "-lboost_system",
                                "-lboost_thread-mt"
                            ]}
                }]
            ]
        }
    ]
}