#include <node.h>
#include <node_buffer.h>
#include <v8.h>
#include <boost/asio.hpp>
#include <iostream>
#include <string>

using namespace v8;
using boost::asio::ip::udp;

using namespace std;

boost::asio::io_service io_service;
udp::socket server_socket(io_service, udp::v4());
char buffer[65535];
udp::endpoint remote_endpoint;
Persistent<Function> on_message;// = Persistent<Function>::New(Null());// = Null();

//Persistent<Object> globalObj = Context::GetCurrent()->Global();
Persistent<Function> bufferConstructor = Persistent<Function>::New(Handle<Function>::Cast(Context::GetCurrent()->Global()->Get(String::New("Buffer"))));

void receive();

Handle<Object> create_buffer(const char* data, const size_t& size)
{
    HandleScope scope;

    node::Buffer* slow_buffer = node::Buffer::New(size);
    memcpy(node::Buffer::Data(slow_buffer), data, size);
    //Local<Object> globalObj = Context::GetCurrent()->Global();
    //Local<Function> bufferConstructor = Local<Function>::Cast(globalObj->Get(String::New("Buffer")));
    Handle<Value> constructorArgs[3] = { slow_buffer->handle_, Integer::New(size), Integer::New(0) };
    Local<Object> actualBuffer = bufferConstructor->NewInstance(3, constructorArgs);

    return scope.Close(actualBuffer);
}

void handle_receive(const boost::system::error_code& error, std::size_t bytes_transferred)
{
    if (error)
    {
        cout << "recv error:" << error.message() << endl;
        return;
    }
    
    // node::Buffer* slow_buffer = node::Buffer::New(bytes_transferred);
    // memcpy(node::Buffer::Data(slow_buffer), buffer, bytes_transferred);
    // v8::Local<v8::Object> globalObj = v8::Context::GetCurrent()->Global();
    // v8::Local<v8::Function> bufferConstructor = v8::Local<v8::Function>::Cast(globalObj->Get(v8::String::New("Buffer")));
    // v8::Handle<v8::Value> constructorArgs[3] = { slow_buffer->handle_, v8::Integer::New(bytes_transferred), v8::Integer::New(0) };
    // v8::Local<v8::Object> actualBuffer = bufferConstructor->NewInstance(3, constructorArgs);
    Local<Object> actualBuffer = Local<Object>::New(create_buffer(buffer, bytes_transferred));

    Local<ObjectTemplate> rinfo = ObjectTemplate::New();
    rinfo->Set(String::New("address"), String::New(remote_endpoint.address().to_string().c_str()));
    rinfo->Set(String::New("port"), Integer::New(remote_endpoint.port()));

    Local<Value> argv[2] = {
        actualBuffer,
        rinfo->NewInstance()
    };

    on_message->Call(Context::GetCurrent()->Global(), 2, argv);
    receive();
}

void receive()
{
    server_socket.async_receive_from(boost::asio::buffer(buffer, 65535), remote_endpoint, handle_receive);
}

void handle_send(const boost::system::error_code& error, std::size_t bytes_transferred)
{
    //cout << "handle_send " << bytes_transferred << endl;
    if (error)
    {
        cout << "error:" << error.message() << endl;
    }
}

Handle<Value> Send(const Arguments& args)
{
    HandleScope scope;

    //cout << "send" << endl;

    if (args.Length() < 5) 
    {
        ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
        return scope.Close(Undefined());
    }

    if (!args[0]->IsObject() || !args[1]->IsNumber() || !args[2]->IsNumber() || !args[3]->IsNumber() || !args[4]->IsString()) 
    {
        ThrowException(Exception::TypeError(String::New("Wrong arguments")));
        return scope.Close(Undefined());
    }

    Local<Object> buf = args[0]->ToObject();
    int begin = args[1]->ToInteger()->Value();
    int end = args[2]->ToInteger()->Value();
    int port = args[3]->ToInteger()->Value();
    string address = *String::AsciiValue(args[4]->ToString());

    server_socket.async_send_to(boost::asio::buffer(node::Buffer::Data(buf) + begin, end - begin), 
        udp::endpoint(boost::asio::ip::address::from_string(address), port), handle_send);

    return scope.Close(Undefined());
}

Handle<Value> Bind(const Arguments& args) 
{
    HandleScope scope;

    if (args.Length() < 2) 
    {
        ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
        return scope.Close(Undefined());
    }

    if (!args[0]->IsString() || !args[1]->IsNumber()) 
    {
        ThrowException(Exception::TypeError(String::New("Wrong arguments")));
        return scope.Close(Undefined());
    }

    string host = *String::AsciiValue(args[0]->ToString());
    int port = args[1]->ToNumber()->Value();

    cout << host << ":" << port << endl;

    try
    {
        server_socket.bind(udp::endpoint(udp::v4(), port));
    }
    catch (const boost::system::system_error& e)
    {
        ThrowException(Exception::TypeError(String::New(e.what())));
        return scope.Close(Undefined());
    }

    //Persistent<Function> bufferConstructor;// = Persistent<Function>::Cast(Context::GetCurrent()->Global()->Get(String::New("Buffer")));

    receive();

    io_service.run();

    return scope.Close(Undefined());
}

Handle<Value> On(const Arguments& args)
{
    HandleScope scope;

    if (args.Length() < 2) 
    {
        ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
        return scope.Close(Undefined());
    }

    if (!args[0]->IsString() || !args[1]->IsFunction()) 
    {
        ThrowException(Exception::TypeError(String::New("Wrong arguments")));
        return scope.Close(Undefined());
    }

    //cout << "1" << on_message->IsNull() << endl;

    string type = *String::AsciiValue(args[0]->ToString());
    if (type == "message")
    {
        //on_message = Persistent<Function>::Cast(args[1]);
        on_message = Persistent<Function>::New(Handle<Function>::Cast(args[1]));
        
        // Local<Value> argv[1] = { Local<Value>::New(String::New("hello world")) };
        // on_message->Call(Context::GetCurrent()->Global(), 0, argv);
    }
    cout << "2 " << on_message->IsNull() << endl;

    return scope.Close(Undefined());
}

void init(Handle<Object> target) {
  NODE_SET_METHOD(target, "bind", Bind);
  NODE_SET_METHOD(target, "on", On);
  NODE_SET_METHOD(target, "send", Send);
}

NODE_MODULE(asio, init);