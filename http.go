package main

import (
	//"fmt"
	"net/http"
)

type MyHandler struct {
	Buffer []byte
}

func NewHandler() *MyHandler {
	handler := MyHandler{Buffer: make([]byte, 1024*10)}
	for i := range handler.Buffer {
		handler.Buffer[i] = 'a'
	}

	return &handler
}

func (h *MyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	//fmt.Println("handle")
	w.WriteHeader(200)
	w.Write(h.Buffer)
}

func main() {
	buf := make([]byte, 1024*10)
	for i := range buf {
		buf[i] = 'a'
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write(buf)
	})

	//handler := NewHandler()

	//http.ListenAndServe(":8000", handler)
	http.ListenAndServe(":8000", nil)
}
