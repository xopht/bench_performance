package main
 
import (
    "fmt"
    "net"
    "time"
    //"strconv"
    "os"
)
 
func CheckError(err error) {
    if err  != nil {
        fmt.Println("Error: " , err)
        os.Exit(1)
    }
}
 
func main() {
    ServerAddr,err := net.ResolveUDPAddr("udp","127.0.0.1:33333")
    CheckError(err)
 
    LocalAddr, err := net.ResolveUDPAddr("udp", "127.0.0.1:33335")
    CheckError(err)
 
    client, err := net.DialUDP("udp", nil, ServerAddr)
    CheckError(err)
 
    defer client.Close()

    server, err := net.ListenUDP("udp", LocalAddr)
    CheckError(err)

    sendBuffer := make([]byte, 2048);
    receiveBuffer := make([]byte, 30000);

    for i, _ := range sendBuffer {
        sendBuffer[i] = 'a'
    }

    bytesSent := 0
    bytesReceived := 0
    start := time.Now()
    for i := 0; i < 50000; i++ {
        n, err := client.Write(sendBuffer)
        if err != nil {
            fmt.Println(err)
        } else {
            bytesSent += n

            n, _ := server.Read(receiveBuffer)
            bytesReceived += n
        }
    }
    elapsed := time.Since(start)

    fmt.Println(bytesSent, bytesReceived, elapsed)
}