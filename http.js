var http = require('http');
var buf = new Buffer(1024 * 10);
buf.fill('a');

http.createServer(function (req, res) {
  res.writeHead(200);
  res.end(buf);
}).listen(8000);